package javax.util.data.datatable.csv;

import javax.util.data.datatable.RowWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CSVWriter implements RowWriter {
    private BufferedWriter writer;

    @Override
    public void initialize(String filePath, List<String> columnNames) throws IOException {
        writer = new BufferedWriter(new FileWriter(filePath));
        writer.write(String.join(",", columnNames));
        writer.newLine();
    }

    @Override
    public void writeRow(Map<String, Object> row) throws IOException {
        String line = row.values().stream()
                .map(Object::toString)
                .collect(Collectors.joining(","));
        writer.write(line);
        writer.newLine();
    }

    @Override
    public void finish() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }
}

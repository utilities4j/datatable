package javax.util.data.datatable.csv;

import javax.util.data.datatable.RowReader;
import com.opencsv.exceptions.CsvValidationException;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Optional;

public class CSVReader implements RowReader {

    private com.opencsv.CSVReader csvReader;
    private String[] headers;
    private String[] currentRow;

    public CSVReader(File file) throws IOException, CsvValidationException {
        this(new FileReader(file));
    }

    public CSVReader(String fileName) throws IOException, CsvValidationException {
        this(new FileReader(fileName));
    }

    public CSVReader(Path filePath) throws IOException, CsvValidationException {
        this(new FileReader(filePath.toFile()));
    }

    public CSVReader(InputStream filePath) throws IOException, CsvValidationException {
        this(convertInputStreamToReader(filePath));
    }

    private CSVReader(FileReader fileReader) throws IOException, CsvValidationException {
        csvReader = new com.opencsv.CSVReader(fileReader);
        headers = csvReader.readNext();
    }

    @Override
    public boolean hasNext() throws IOException {
        try {
            currentRow = csvReader.readNext();
            return currentRow != null;
        } catch (CsvValidationException e) {
            throw new IOException("Failed to read CSV file", e);
        }
    }

    @Override
    public boolean next() throws IOException {
        //@TODO Implementar a lógica de leitura da próxima linha
        if (!hasNext()) {
            throw new IOException("No more rows");
        }
        return true;
    }

    @Override
    public int getRowNumber() {
        throw new UnsupportedOperationException("Not supported yet."); //@TODO implementar
    }

    @Override
    public boolean columnNameExists(String columnName) {
        for (String header : headers) {
            if (header.equals(columnName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Optional<String> verifyColumnsNames(String... columnNames) {
        throw new UnsupportedOperationException("Not supported yet.");  //@TODO implementar
    }

    @Override
    public Object get(String columnName) {
        throw new UnsupportedOperationException("Not supported yet.");  //@TODO implementar
    }

    @Override
    public String getAsString(String columnName) {
        return getValue(columnName);
    }

    @Override
    public String getAsString(String columnName, String defaultvalue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Integer getAsInteger(String columnName) {
        String value = getValue(columnName);
        return value != null ? Integer.valueOf(value) : null;
    }

    @Override
    public Integer getAsInteger(String columnName, Integer defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); //@TODO implementar
    }

    @Override
    public Long getAsLong(String columnName) {
        String value = getValue(columnName);
        return value != null ? Long.valueOf(value) : null;
    }

    @Override
    public Float getAsFloat(String columnName) {
        String value = getValue(columnName);
        return value != null ? Float.valueOf(value) : null;
    }

    @Override
    public Double getAsDouble(String columnName) {
        String value = getValue(columnName);
        return value != null ? Double.valueOf(value) : null;
    }

    @Override
    public BigInteger getAsBigInteger(String columnName) {
        String value = getValue(columnName);
        return value != null ? new BigInteger(value) : null;
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName) {
        String value = getValue(columnName);
        return value != null ? new BigDecimal(value) : null;
    }

    @Override
    public LocalDate getAsLocalDate(String columnName) {
        return getAsLocalDate(columnName, null);
    }

    @Override
    public LocalDate getAsLocalDate(String columnName, LocalDate defaultValue) {
        String value = getValue(columnName);
        if (value != null) {
            try {
                return LocalDate.parse(value, DateTimeFormatter.ISO_LOCAL_DATE);
            } catch (DateTimeParseException e) {
                throw new RuntimeException(e);
            }
        }
        return defaultValue;
    }

    @Override
    public LocalDateTime getAsDateTime(String columnName) {
        String value = getValue(columnName);
        if (value != null) {
            try {
                return LocalDateTime.parse(value, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            } catch (DateTimeParseException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    private String getValue(String columnName) {
        for (int i = 0; i < headers.length; i++) {
            if (headers[i].equals(columnName)) {
                return currentRow[i];
            }
        }
        return null;
    }

    private static FileReader convertInputStreamToReader(InputStream inputStream) throws IOException {
        // Converte o InputStream para uma String
        StringBuilder textBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                textBuilder.append(line).append(System.lineSeparator());
            }
        }

        return new FileReader(textBuilder.toString());
    }

    @Override
    public Long getAsLong(String columnName, Long defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Float getAsFloat(String columnName, Float defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Double getAsDouble(String columnName, Double defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BigInteger getAsBigInteger(String columnName, BigInteger defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName, BigDecimal defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Date getAsDate(String columnName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Date getAsDate(String columnName, Date deafultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

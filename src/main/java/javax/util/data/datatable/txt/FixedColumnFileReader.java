package javax.util.data.datatable.txt;

import javax.util.data.datatable.RowReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Map;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class FixedColumnFileReader implements RowReader {

    private BufferedReader reader;
    private Iterator<String> iterator;
    private String currentRow;
    private Map<String, Integer[]> columnPositions;

    public FixedColumnFileReader(File file, Map<String, Integer[]> columnPositions) throws IOException {
        this.reader = new BufferedReader(new FileReader(file));
        this.columnPositions = columnPositions;
    }

    public FixedColumnFileReader(String fileName, Map<String, Integer[]> columnPositions) throws IOException {
        this(new File(fileName), columnPositions);
    }

    public FixedColumnFileReader(Path filePath, Map<String, Integer[]> columnPositions) throws IOException {
        this(filePath.toFile(), columnPositions);
    }

    @Override
    public boolean hasNext() throws IOException {
        if (iterator == null) {
            iterator = reader.lines().iterator();
        }
        return iterator.hasNext();
    }

    @Override
    public boolean next() throws IOException {
        if (hasNext()) {
            currentRow = iterator.next();
            return true;
        }
        return false;
    }

    @Override
    public int getRowNumber() {
        throw new UnsupportedOperationException("Not supported yet."); //@TODO implementar
    }

    @Override
    public boolean columnNameExists(String columnName) {
        return columnPositions.get(columnName) != null;
    }

    @Override
    public Optional<String> verifyColumnsNames(String... columnNames) {
        List<String> namesNotFound = new ArrayList<>() {
            @Override
            public String toString() {
                return String.join(",", this);
            }
        };

        for (String nome : columnNames) {
            if (!columnPositions.containsKey(nome.toUpperCase())) {
                namesNotFound.add(nome);
            }
        }

        return namesNotFound.isEmpty() ? Optional.empty() : Optional.of(namesNotFound.toString());
    }

    private String getColumnValue(String columnName) {
        if (columnPositions == null) {
            return currentRow;
        }
        Integer[] positions = columnPositions.get(columnName);
        if (positions == null || currentRow == null) {
            return null;
        }
        int start = positions[0];
        int end = positions[1];
        if (start >= currentRow.length()) {
            return null;
        }
        return currentRow.substring(start, Math.min(end, currentRow.length())).trim();
    }

    @Override
    public Object get(String columnName) {
        throw new UnsupportedOperationException("Not supported yet.");  //@TODO implementar
    }

    @Override
    public String getAsString(String columnName) {
        return getColumnValue(columnName);
    }

    @Override
    public String getAsString(String columnName, String defaultvalue) {
        throw new UnsupportedOperationException("Not supported yet."); //@TODO implementar
    }

    @Override
    public Integer getAsInteger(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? Integer.valueOf(value) : null;
    }

    @Override
    public Integer getAsInteger(String columnName, Integer defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); //@TODO implementar
    }

    @Override
    public Long getAsLong(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? Long.valueOf(value) : null;
    }

    @Override
    public Float getAsFloat(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? Float.valueOf(value) : null;
    }

    @Override
    public Double getAsDouble(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? Double.valueOf(value) : null;
    }

    @Override
    public BigInteger getAsBigInteger(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? new BigInteger(value) : null;
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? new BigDecimal(value) : null;
    }

    @Override
    public LocalDate getAsLocalDate(String columnName) {
        return getAsLocalDate(columnName, null);
    }

    @Override
    public LocalDate getAsLocalDate(String columnName, LocalDate defaultValue) {
        String value = getColumnValue(columnName);
        return value != null ? LocalDate.parse(value, DateTimeFormatter.ISO_DATE) : defaultValue;
    }

    @Override
    public LocalDateTime getAsDateTime(String columnName) {
        String value = getColumnValue(columnName);
        return value != null ? LocalDateTime.parse(value, DateTimeFormatter.ISO_DATE_TIME) : null;
    }

    @Override
    public Long getAsLong(String columnName, Long defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Float getAsFloat(String columnName, Float defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Double getAsDouble(String columnName, Double defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public BigInteger getAsBigInteger(String columnName, BigInteger defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName, BigDecimal defaultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Date getAsDate(String columnName) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Date getAsDate(String columnName, Date deafultValue) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
}

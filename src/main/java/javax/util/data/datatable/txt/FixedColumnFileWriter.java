package javax.util.data.datatable.txt;

import javax.util.data.datatable.RowWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FixedColumnFileWriter implements RowWriter {

    private BufferedWriter writer;
    private final int[] columnWidths;

    public FixedColumnFileWriter(int[] columnWidths) {
        this.columnWidths = columnWidths;
    }

    @Override
    public void initialize(String filePath, List<String> columnNames) throws IOException {
        writer = new BufferedWriter(new FileWriter(filePath));
        writeFixedWidthLine(columnNames);
    }

    @Override
    public void writeRow(Map<String, Object> row) throws IOException {
        writeFixedWidthLine(row.values().stream().map(Object::toString).toList());
    }

    @Override
    public void finish() throws IOException {
        if (writer != null) {
            writer.close();
        }
    }

    private void writeFixedWidthLine(List<?> values) throws IOException {
        for (int i = 0; i < values.size(); i++) {
            String value = values.get(i).toString();
            writer.write(padRight(value, columnWidths[i]));
        }
        writer.newLine();
    }

    private String padRight(String value, int length) {
        if (value.length() > length) {
            return value.substring(0, length);
        }
        return String.format("%-" + length + "s", value);
    }
}

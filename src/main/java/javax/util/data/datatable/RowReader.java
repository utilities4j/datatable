package javax.util.data.datatable;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

public interface RowReader {

    boolean hasNext() throws IOException;

    boolean next() throws IOException;
    
    int getRowNumber();
    
    boolean columnNameExists(String columnName);
    
    Optional<String> verifyColumnsNames(String... columnNames);
    
    Object get(String columnName);

    String getAsString(String columnName);
    
    String getAsString(String columnName, String defaultvalue);
        
    Integer getAsInteger(String columnName);
    
    Integer getAsInteger(String columnName, Integer defaultValue);

    Long getAsLong(String columnName);
    
    Long getAsLong(String columnName, Long defaultValue);

    Float getAsFloat(String columnName);
    
    Float getAsFloat(String columnName, Float defaultValue);

    Double getAsDouble(String columnName);
    
    Double getAsDouble(String columnName, Double defaultValue);

    BigInteger getAsBigInteger(String columnName);
    
    BigInteger getAsBigInteger(String columnName, BigInteger defaultValue);

    BigDecimal getAsBigDecimal(String columnName);
    
    BigDecimal getAsBigDecimal(String columnName, BigDecimal defaultValue);

    Date getAsDate(String columnName);
    
    Date getAsDate(String columnName, Date deafultValue);
    
    LocalDate getAsLocalDate(String columnName);
    
    LocalDate getAsLocalDate(String columnName, LocalDate defaultValue);

    LocalDateTime getAsDateTime(String columnName);
}

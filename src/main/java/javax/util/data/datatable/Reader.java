package javax.util.data.datatable;

import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import static javax.util.data.datatable.FileDetector.FileType.XLS;
import static javax.util.data.datatable.FileDetector.FileType.XLSX;
import javax.util.data.datatable.csv.CSVReader;
import javax.util.data.datatable.excel.ExcelReader;
import javax.util.data.datatable.txt.FixedColumnFileReader;

public class Reader implements RowReader {

    private RowReader rowReader;

    // Construtor com File sem columnPositions
    public Reader(File file) throws IOException, CsvValidationException {
        initializeReader(file, null);
    }

    // Construtor com File e columnPositions
    public Reader(File file, Map<String, Integer[]> columnPositions) throws IOException, CsvValidationException {
        initializeReader(file, columnPositions);
    }

    // Construtor com fileName sem columnPositions
    public Reader(String fileName) throws IOException, CsvValidationException {
        this(new File(fileName), null);
    }

    // Construtor com fileName e columnPositions
    public Reader(String fileName, Map<String, Integer[]> columnPositions) throws IOException, CsvValidationException {
        this(new File(fileName), columnPositions);
    }

    // Construtor com filePath sem columnPositions
    public Reader(Path filePath) throws IOException, CsvValidationException {
        this(filePath.toFile(), null);
    }

    // Construtor com filePath e columnPositions
    public Reader(Path filePath, Map<String, Integer[]> columnPositions) throws IOException, CsvValidationException {
        this(filePath.toFile(), columnPositions);
    }

    public Reader(InputStream inputStream) throws IOException, CsvValidationException {
        initializeReader(inputStream);
    }

    private void initializeReader(File file, Map<String, Integer[]> columnPositions) throws IOException, CsvValidationException {
        String fileName = file.getName();
        if (fileName.endsWith(".csv")) {
            rowReader = new CSVReader(file);
        } else if (fileName.endsWith(".xlsx") || fileName.endsWith(".xls")) {
            rowReader = new ExcelReader(file);
        } else if (fileName.endsWith(".txt")) {
            rowReader = new FixedColumnFileReader(file, columnPositions);
        } else {
            throw new IOException("Unsupported file format: " + fileName);
        }
    }

    private void initializeReader(InputStream inputStream) throws IOException, CsvValidationException {
        FileDetector.FileType fileType = FileDetector.getType(inputStream);
        switch (fileType) {
            case CSV ->
                rowReader = new CSVReader(inputStream);
            case XLSX ->
                rowReader = new ExcelReader(inputStream, false);
            case XLS ->
                rowReader = new ExcelReader(inputStream, true);
            //case TXT -> rowReader = new FixedWidthReader(inputStream, columnPositions);//@TODO
            default ->
                throw new IOException("Unsupported file format");
        }
    }

    @Override
    public boolean hasNext() throws IOException {
        return rowReader.hasNext();
    }

    @Override
    public boolean next() throws IOException {
        return rowReader.next();
    }

    @Override
    public int getRowNumber() {
        return rowReader.getRowNumber();
    }

    @Override
    public boolean columnNameExists(String columnName) {
        return rowReader.columnNameExists(columnName);
    }

    @Override
    public Optional<String> verifyColumnsNames(String... columnNames) {
        return rowReader.verifyColumnsNames(columnNames);
    }

    @Override
    public Object get(String columnName) {
        return rowReader.get(columnName);
    }

    @Override
    public String getAsString(String columnName) {
        return rowReader.getAsString(columnName);
    }

    @Override
    public String getAsString(String columnName, String defaultvalue) {
        return rowReader.getAsString(columnName, defaultvalue);
    }

    @Override
    public Integer getAsInteger(String columnName) {
        return rowReader.getAsInteger(columnName);
    }

    @Override
    public Integer getAsInteger(String columnName, Integer defaultValue) {
        return rowReader.getAsInteger(columnName, defaultValue);
    }

    @Override
    public Long getAsLong(String columnName) {
        return rowReader.getAsLong(columnName);
    }

    @Override
    public Float getAsFloat(String columnName) {
        return rowReader.getAsFloat(columnName);
    }

    @Override
    public Double getAsDouble(String columnName) {
        return rowReader.getAsDouble(columnName);
    }

    @Override
    public BigInteger getAsBigInteger(String columnName) {
        return rowReader.getAsBigInteger(columnName);
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName) {
        return rowReader.getAsBigDecimal(columnName);
    }

    @Override
    public LocalDate getAsLocalDate(String columnName) {
        return rowReader.getAsLocalDate(columnName);
    }

    @Override
    public LocalDateTime getAsDateTime(String columnName) {
        return rowReader.getAsDateTime(columnName);
    }

    public static void main(String[] args) {
        readerExcel(args);
    }

    public static void readerExcel(String[] args) {
//        try {
//            Reader reader = new Reader("example.txt", columnPositions);
//            while (reader.hasNext()) {
//                reader.next();
//                System.out.println("Name: " + reader.getAsString("Name"));
//                System.out.println("Age: " + reader.getAsInteger("Age"));
//                System.out.println("Birthdate: " + reader.getAsDate("Birthdate"));
//            }
//
//            // Exemplo para arquivos .txt sem colunas de largura fixa (retorna a linha toda)
//            Reader readerWithoutColumns = new Reader("example_no_columns.txt");
//            while (readerWithoutColumns.hasNext()) {
//                readerWithoutColumns.next();
//                System.out.println("Line: " + readerWithoutColumns.getAsString(null)); // Nome da coluna é irrelevante
//            }
//        } catch (IOException | CsvValidationException e) {
//            e.printStackTrace();
//        }
    }

    public static void readerTxt(String[] args) {
        try {
            // Exemplo para arquivos .txt com colunas de largura fixa
            Map<String, Integer[]> columnPositions = Map.of(
                    "Name", new Integer[]{0, 10},
                    "Age", new Integer[]{10, 15},
                    "Birthdate", new Integer[]{15, 25}
            );

            RowReader reader = new Reader("example.txt", columnPositions);
            while (reader.hasNext()) {
                reader.next();
                System.out.println("Name: " + reader.getAsString("Name"));
                System.out.println("Age: " + reader.getAsInteger("Age"));
                System.out.println("Birthdate: " + reader.getAsLocalDate("Birthdate"));
            }

            // Exemplo para arquivos .txt sem colunas de largura fixa (retorna a linha toda)
            Reader readerWithoutColumns = new Reader("example_no_columns.txt");
            while (readerWithoutColumns.hasNext()) {
                readerWithoutColumns.next();
                System.out.println("Line: " + readerWithoutColumns.getAsString(null)); // Nome da coluna é irrelevante
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
    }

    public static void readerTxtWithoutColumns(String[] args) {
        try {
            RowReader readerWithoutColumns = new Reader("example_no_columns.txt") {
            };
            while (readerWithoutColumns.hasNext()) {
                readerWithoutColumns.next();
                System.out.println("Line: " + readerWithoutColumns.getAsString(null)); // Nome da coluna é irrelevante
            }
        } catch (IOException | CsvValidationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Long getAsLong(String columnName, Long defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Float getAsFloat(String columnName, Float defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Double getAsDouble(String columnName, Double defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BigInteger getAsBigInteger(String columnName, BigInteger defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName, BigDecimal defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Date getAsDate(String columnName) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Date getAsDate(String columnName, Date deafultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public LocalDate getAsLocalDate(String columnName, LocalDate defaultValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

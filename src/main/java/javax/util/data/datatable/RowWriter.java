package javax.util.data.datatable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface RowWriter {

    void initialize(String filePath, List<String> columnNames) throws IOException;

    void writeRow(Map<String, Object> row) throws IOException;

    void finish() throws IOException;
}

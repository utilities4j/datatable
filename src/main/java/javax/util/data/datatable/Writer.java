package javax.util.data.datatable;

import javax.util.data.datatable.RowWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.util.data.datatable.csv.CSVWriter;
import javax.util.data.datatable.excel.ExcelWriter;
import javax.util.data.datatable.txt.FixedColumnFileWriter;

public class Writer {

    private final List<String> columnNames;
    final private List<Map<String, Object>> rows = new ArrayList<>();
    private final Map<String, Object> currentRow = new LinkedHashMap<>();
    private final RowWriter dataHandler;
    private final String filename;

    public Writer(List<String> columnNames, String filename) throws IllegalArgumentException {
        this.columnNames = columnNames;
        this.filename = filename;
        this.dataHandler = selectHandler(filename);
    }

    private RowWriter selectHandler(String filePath) throws IllegalArgumentException {
        if (filePath.endsWith(".csv")) {
            return new CSVWriter();
        } else if (filePath.endsWith(".xlsx")) {
            return new ExcelWriter();
        } else if (filePath.endsWith(".txt")) {
            return new FixedColumnFileWriter(new int[]{10, 5, 20}); // Exemplo de largura fixa, ajuste conforme necessário
        } else {
            throw new IllegalArgumentException("Formato de arquivo não suportado: " + filePath);
        }
    }

    public void addValue(String columnName, Object value) {
        if (!columnNames.contains(columnName)) {
            throw new IllegalArgumentException("Coluna não existe: " + columnName);
        }
        currentRow.put(columnName, value);
    }

    public void addRow() {
        rows.add(new LinkedHashMap<>(currentRow));
        currentRow.clear();
    }

    public void save() {
        try {
            dataHandler.initialize(filename, columnNames);
            for (Map<String, Object> row : rows) {
                dataHandler.writeRow(row);
            }
            dataHandler.finish();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static void main(String[] args) {
        List<String> columns = List.of("Nome", "Idade", "Email");
        try {
            Writer dataTable = new Writer(columns, "output.xlsx");

            dataTable.addValue("Nome", "João");
            dataTable.addValue("Idade", 30);
            dataTable.addValue("Email", "joao@example.com");
            dataTable.addRow();

            dataTable.addValue("Nome", "Maria");
            dataTable.addValue("Idade", 25);
            dataTable.addValue("Email", "maria@example.com");
            dataTable.addRow();

            dataTable.addValue("Nome", "Pedro");
            dataTable.addValue("Idade", 35);
            dataTable.addValue("Email", "pedro@example.com");
            dataTable.addRow();

            dataTable.save();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}

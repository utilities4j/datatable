package javax.util.data.datatable;

import java.text.Normalizer;

/**
 *
 * @author Marcius
 */
public class UtilString {

    public static String higienizacao(String columnName) {
        String value = columnName.replaceAll("\\[.*?\\]", "")// Remove conteúdo entre colchetes e os próprios colchetes
                .replaceAll("\\r?\\n", "")// Remove caracteres de quebra de linha
                .replaceAll("\\s+", " ") // Substitui múltiplos espaços consecutivos por um único espaço
                .trim()// Remove espaços iniciais e finais
                .replaceAll(" ", "_");// Substitui todos os espaços por underscores

        return removerAcentuacao(value).toUpperCase();
    }

    public static String removerAcentuacao(String texto) {
        // Normaliza a string para decompor caracteres acentuados
        String textoNormalizado = Normalizer.normalize(texto, Normalizer.Form.NFD);
        // Remove os caracteres não ASCII (acentos)
        textoNormalizado = textoNormalizado.replaceAll("[^\\p{ASCII}]", "");
        return textoNormalizado;
    }

    public static boolean ifBlankThenNull(String value) {
        return (value == null || value.replaceAll("[^\\x20-\\x7E]", "").isBlank());
    }
}

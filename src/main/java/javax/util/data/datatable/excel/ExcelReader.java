package javax.util.data.datatable.excel;

import javax.util.data.datatable.RowReader;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.util.data.datatable.UtilString;
import static org.apache.poi.ss.usermodel.CellType.BLANK;
import static org.apache.poi.ss.usermodel.CellType.BOOLEAN;
import static org.apache.poi.ss.usermodel.CellType.FORMULA;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;

public class ExcelReader implements RowReader {

    private Workbook workbook;
    private Sheet sheet;
    private Iterator<Row> rowIterator;
    private Map<String, Integer> columnsHeaderMap;
    private Row currentRow;

    public ExcelReader(File file) throws IOException {
        this(new FileInputStream(file), file.getName().endsWith(".xls"));
    }

    public ExcelReader(String fileName) throws IOException {
        this(abrirInputStream(fileName), fileName.endsWith(".xls"));
    }

    /**
     * Método auxiliar para tentar abrir o arquivo a partir do sistema de arquivos ou dos recursos.
     */
    private static InputStream abrirInputStream(String fileName) throws IOException {
        // Tenta carregar o arquivo do sistema de arquivos
        File file = new File(fileName);

        if (file.exists()) {
            // Se o arquivo existe no sistema de arquivos, usa FileInputStream
            return new FileInputStream(file);
        } else {
            // Caso o arquivo não exista no sistema de arquivos, tenta carregar do classpath (resources)
            InputStream inputStream = ExcelReader.class.getClassLoader().getResourceAsStream(fileName);

            if (inputStream == null) {
                throw new IOException("Arquivo não encontrado: " + fileName);
            }

            return inputStream;
        }
    }

    public ExcelReader(Path filePath) throws IOException {
        this(filePath.toFile());
    }

    public ExcelReader(InputStream fileInputStream) throws IOException {
        try {
            workbook = new XSSFWorkbook(fileInputStream); // Tenta inicializar com XSSFWorkbook
        } catch (IOException e) {
            fileInputStream.reset(); // Reinicia o InputStream, se possível
            workbook = new HSSFWorkbook(fileInputStream);
        }

        if (workbook == null) {
            throw new IOException("Unsupported file format");
        }
        initialize();
    }

    public ExcelReader(InputStream fileInputStream, boolean isXLS) throws IOException {
        if (isXLS) {
            workbook = new HSSFWorkbook(fileInputStream);
        } else {
            workbook = new XSSFWorkbook(fileInputStream);
        }

        initialize();
    }

    //<editor-fold defaultstate="collapsed" desc="Métodos de gerenciamento de colunas">
    private void initialize() {
        sheet = workbook.getSheetAt(0);
        rowIterator = sheet.iterator();
        if (rowIterator.hasNext()) {
            Row headerRow = rowIterator.next();
            columnsHeaderMap = new HashMap<>();

            int numberOfCells = headerRow.getLastCellNum(); // Get the number of cells in the row
            for (int cellIndex = 0; cellIndex < numberOfCells; cellIndex++) {
                Cell cell = headerRow.getCell(cellIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                String value = cellValueAsString(cell, "");
                if (!value.isEmpty()) {
                    columnsHeaderMap.put(value.toUpperCase(), cell.getColumnIndex());
                    columnsHeaderMap.put(UtilString.higienizacao(value), cell.getColumnIndex());
                }
            }
        }
    }

    @Override
    public boolean columnNameExists(String columnName) {
        return columnsHeaderMap.get(columnName.toUpperCase()) != null;
    }

    @Override
    public Optional<String> verifyColumnsNames(String... columnNames) {
        List<String> namesNotFound = new ArrayList<>() {
            @Override
            public String toString() {
                return String.join(",", this);
            }
        };

        for (String nome : columnNames) {
            if (!columnsHeaderMap.containsKey(nome.toUpperCase())) {
                namesNotFound.add(nome);
            }
        }

        return namesNotFound.isEmpty() ? Optional.empty() : Optional.of(namesNotFound.toString());
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="implementação do Iterable">
    @Override
    public boolean hasNext() {
        return rowIterator.hasNext();
    }

    @Override
    public boolean next() {
        if (hasNext()) {
            currentRow = rowIterator.next();
            return true;
        }
        return false;
    }

    @Override
    public int getRowNumber() {
        return currentRow.getRowNum();
    }
    //</editor-fold>

    private Cell findCell(String columnName) throws NoSuchFieldError {
        Integer columnIndex = columnsHeaderMap.get(columnName.toUpperCase());
        if (columnIndex == null) {
            throw new NoSuchFieldError("Field " + columnName);
        }
        return currentRow.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
    }

    private Object getCellValue(String columnName) {
        Cell cell = findCell(columnName);
        return getCellValue(cell);
    }

    private Object getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        return switch (cell.getCellType()) {
            case STRING ->
                cell.getStringCellValue();
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    yield cell.getDateCellValue();
                } else {
                    yield cell.getNumericCellValue();
                }
            }
            case BOOLEAN ->
                cell.getBooleanCellValue();
            case FORMULA ->
                cell.getCellFormula();
            case BLANK ->
                null;
            default ->
                throw new IllegalArgumentException("Tipo de célula desconhecido: " + cell.getCellType());
        };
    }

    private String cellValueAsString(Cell cell, String valueDefault) {
        switch (cell.getCellType()) {
            case STRING -> {
                // caracteres permitidos
                //Letras (incluindo acentuadas): \p{L}
                //Números: \p{N}
                //Espaços: \s
                //Pontuações: \p{P}
                return cell.getStringCellValue().replaceAll("[^\\p{L}\\p{N}\\p{P}\\s]", "");
            }
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                } else {
                    double numericValue = cell.getNumericCellValue();
                    if (numericValue == (long) numericValue) {
                        return String.valueOf((long) numericValue);
                    } else {
                        return String.valueOf(numericValue);
                    }
                }
            }
            case BOOLEAN -> {
                return Boolean.toString(cell.getBooleanCellValue());
            }
            case FORMULA -> {
                // Evaluating formula result as a string
                FormulaEvaluator evaluator = currentRow.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                return evaluator.evaluate(cell).formatAsString();
            }
            case BLANK -> {//A célula existe no Excel, mas não contém nenhum dado.
                return valueDefault;
            }
            default -> {
                return cell.toString();
            }
        }
    }

    private Integer cellValueAsInteger(Cell cell, Integer defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    double numericValue = cell.getNumericCellValue();
                    // Truncar número decimal e retornar como inteiro
                    return (int) numericValue; // Isso já trunca a parte decimal
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Limpa a string, removendo caracteres não numéricos, e tenta converter para Integer
                    String cleanedValue = cellValue.replaceAll("[^0-9]", "");
                    return cleanedValue.isEmpty() ? defaultValue : Integer.valueOf(cleanedValue);
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para Integer (true -> 1, false -> 0)
                return cell.getBooleanCellValue() ? 1 : 0;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    double numericValue = evaluatedValue.getNumberValue();
                    // Trunca número decimal e retorna como inteiro
                    return (int) numericValue;
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private Long cellValueAsLong(Cell cell, Long defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    double numericValue = cell.getNumericCellValue();
                    // Truncar número decimal e retornar como long
                    return (long) numericValue; // Isso já trunca a parte decimal
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Limpa a string, removendo caracteres não numéricos, e tenta converter para Long
                    String cleanedValue = cellValue.replaceAll("[^0-9]", "");
                    return cleanedValue.isEmpty() ? defaultValue : Long.valueOf(cleanedValue);
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para Long (true -> 1, false -> 0)
                return cell.getBooleanCellValue() ? 1L : 0L;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    double numericValue = evaluatedValue.getNumberValue();
                    // Trunca número decimal e retorna como long
                    return (long) numericValue;
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private Float cellValueAsFloat(Cell cell, Float defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    double numericValue = cell.getNumericCellValue();
                    // Retornar o valor numérico como float, truncando a parte decimal
                    return (float) numericValue; // Converte diretamente para float
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Limpa a string, removendo caracteres não numéricos e tenta converter para Float
                    String cleanedValue = cellValue.replaceAll("[^0-9.-]", "");
                    return cleanedValue.isEmpty() ? defaultValue : Float.valueOf(cleanedValue);
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para Float (true -> 1.0f, false -> 0.0f)
                return cell.getBooleanCellValue() ? 1.0f : 0.0f;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    double numericValue = evaluatedValue.getNumberValue();
                    // Retorna o número como float
                    return (float) numericValue;
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private Double cellValueAsDouble(Cell cell, Double defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    // Retorna o valor numérico como double
                    return cell.getNumericCellValue();
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Limpa a string removendo caracteres não numéricos, tentando converter para Double
                    String cleanedValue = cellValue.replaceAll("[^0-9.-]", "");
                    return cleanedValue.isEmpty() ? defaultValue : Double.valueOf(cleanedValue);
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para Double (true -> 1.0, false -> 0.0)
                return cell.getBooleanCellValue() ? 1.0 : 0.0;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    // Retorna o número como double
                    return evaluatedValue.getNumberValue();
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private BigInteger cellValueAsBigInteger(Cell cell, BigInteger defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    // Retorna o valor numérico como BigInteger, truncando a parte decimal
                    double numericValue = cell.getNumericCellValue();
                    return BigInteger.valueOf((long) numericValue); // Truncando o valor decimal
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Tenta limpar a string e converter para BigInteger
                    String cleanedValue = cellValue.replaceAll("[^0-9]", "");
                    return cleanedValue.isEmpty() ? defaultValue : new BigInteger(cleanedValue);
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para BigInteger (true -> 1, false -> 0)
                return cell.getBooleanCellValue() ? BigInteger.ONE : BigInteger.ZERO;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    // Trunca número decimal e retorna como BigInteger
                    long numericValue = (long) evaluatedValue.getNumberValue();
                    return BigInteger.valueOf(numericValue);
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private BigDecimal cellValueAsBigDecimal(Cell cell, BigDecimal defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Não queremos tratar datas, então retornamos null
                    return null;
                } else {
                    // Retorna o valor numérico como BigDecimal
                    double numericValue = cell.getNumericCellValue();
                    return BigDecimal.valueOf(numericValue); // Converte o valor numérico para BigDecimal
                }
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                try {
                    // Tenta limpar a string e converter para BigDecimal
                    String cleanedValue = cellValue.replaceAll("[^0-9.,]", "");
                    if (cleanedValue.isEmpty()) {
                        return defaultValue;
                    }
                    // Converte para BigDecimal, considerando vírgulas ou pontos como separadores decimais
                    return new BigDecimal(cleanedValue.replaceAll(",", "."));
                } catch (NumberFormatException e) {
                    return defaultValue; // Se falhar, retorna o valor default
                }
            }
            case BOOLEAN -> {
                // Converte valores booleanos para BigDecimal (true -> 1, false -> 0)
                return cell.getBooleanCellValue() ? BigDecimal.ONE : BigDecimal.ZERO;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como número
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);

                if (evaluatedValue.getCellType() == CellType.NUMERIC) {
                    // Retorna o valor numérico como BigDecimal
                    double numericValue = evaluatedValue.getNumberValue();
                    return BigDecimal.valueOf(numericValue); // Converte o valor numérico para BigDecimal
                }
                return defaultValue; // Se não for numérico, retorna valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private LocalDate cellValueAsLocalDate(Cell cell, LocalDate defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        // Define os DateTimeFormatter para os formatos desejados
        DateTimeFormatter[] dateFormatters = {
            DateTimeFormatter.ISO_DATE, // Formato: yyyy-MM-dd
            DateTimeFormatter.ofPattern("dd/MM/yyyy"), // Formato: dd/MM/yyyy
            DateTimeFormatter.ofPattern("dd.MM.yyyy") // Formato: dd.MM.yyyy
        };

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // A célula é uma data, então convertemos para LocalDate
                    return cell.getDateCellValue().toInstant()
                            .atZone(java.time.ZoneId.systemDefault())
                            .toLocalDate();
                }
                return defaultValue;
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                for (DateTimeFormatter formatter : dateFormatters) {
                    try {
                        return LocalDate.parse(cellValue, formatter);
                    } catch (Exception e) {
                        // Se não puder converter com esse formato, tenta o próximo
                    }
                }
                // Se não conseguir converter com nenhum formato, retorna o valor default
                return defaultValue;
            }
            case BOOLEAN -> {
                // Não tratamos valores booleanos como LocalDate, retornamos o valor default
                return defaultValue;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como data
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);
                if (evaluatedValue.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                    // Converte para LocalDate se for uma data
                    return cell.getDateCellValue().toInstant()
                            .atZone(java.time.ZoneId.systemDefault())
                            .toLocalDate();
                }
                return defaultValue; // Se não for uma data, retorna o valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private Date cellValueAsDate(Cell cell, Date defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        // Define os DateTimeFormatter para os formatos desejados
        SimpleDateFormat[] dateFormatters = {
            new SimpleDateFormat("yyyy-MM-dd"), // Formato: yyyy-MM-dd
            new SimpleDateFormat("dd/MM/yyyy"), // Formato: dd/MM/yyyy
            new SimpleDateFormat("dd.MM.yyyy") // Formato: dd.MM.yyyy
        };

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Converte diretamente para java.util.Date
                    return cell.getDateCellValue();
                }
                return defaultValue;
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                for (SimpleDateFormat formatter : dateFormatters) {
                    try {
                        // Tenta converter com cada formato
                        return formatter.parse(cellValue);
                    } catch (Exception e) {
                        // Se não puder converter com esse formato, tenta o próximo
                    }
                }
                // Se não conseguir converter com nenhum formato, retorna o valor default
                return defaultValue;
            }
            case BOOLEAN -> {
                // Para booleanos, retornamos o valor default, pois não é uma data
                return defaultValue;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como data
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);
                if (evaluatedValue.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                    // Converte para java.util.Date se for uma data
                    return cell.getDateCellValue();
                }
                return defaultValue; // Se não for uma data, retorna o valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    private LocalDateTime cellValueAsLocalDateTime(Cell cell, LocalDateTime defaultValue) {
        if (cell == null || cell.getCellType() == CellType.BLANK) {
            return defaultValue;
        }

        // Define os DateTimeFormatter para os formatos desejados
        DateTimeFormatter[] dateFormatters = {
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"), // Formato: yyyy-MM-dd HH:mm:ss
            DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"), // Formato: dd/MM/yyyy HH:mm:ss
            DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss") // Formato: dd.MM.yyyy HH:mm:ss
        };

        switch (cell.getCellType()) {
            case NUMERIC -> {
                if (DateUtil.isCellDateFormatted(cell)) {
                    // Converte a data para LocalDateTime
                    return cell.getLocalDateTimeCellValue();
                }
                return defaultValue;
            }
            case STRING -> {
                String cellValue = cell.getStringCellValue().trim();
                for (DateTimeFormatter formatter : dateFormatters) {
                    try {
                        // Tenta converter a string para LocalDateTime usando os diferentes formatos
                        return LocalDateTime.parse(cellValue, formatter);
                    } catch (Exception e) {
                        // Se não puder converter com esse formato, tenta o próximo
                    }
                }
                // Se não conseguir converter com nenhum formato, retorna o valor default
                return defaultValue;
            }
            case BOOLEAN -> {
                // Para booleanos, retornamos o valor default, pois não é uma data e hora
                return defaultValue;
            }
            case FORMULA -> {
                // Avalia a fórmula e tenta tratar como data e hora
                FormulaEvaluator evaluator = cell.getSheet().getWorkbook().getCreationHelper().createFormulaEvaluator();
                CellValue evaluatedValue = evaluator.evaluate(cell);
                if (evaluatedValue.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                    // Converte para LocalDateTime se for uma data
                    return cell.getLocalDateTimeCellValue();
                }
                return defaultValue; // Se não for uma data, retorna o valor default
            }
            default -> {
                return defaultValue;
            }
        }
    }

    @Override
    public Object get(String columnName) {
        return getCellValue(columnName);
    }

    @Override
    public String getAsString(String columnName) {
        return getAsString(columnName, null);
    }

    @Override
    public String getAsString(String columnName, String defaultvalue) {
        return cellValueAsString(findCell(columnName), defaultvalue);
    }

    @Override
    public Integer getAsInteger(String columnName) {
        return getAsInteger(columnName, null);
    }

    @Override
    public Integer getAsInteger(String columnName, Integer defaultValue) {
        return cellValueAsInteger(findCell(columnName), defaultValue);
    }

    @Override
    public Long getAsLong(String columnName) {
        return cellValueAsLong(findCell(columnName), null);
    }

    @Override
    public Long getAsLong(String columnName, Long defaultValue) {
        return cellValueAsLong(findCell(columnName), defaultValue);
    }

    @Override
    public Float getAsFloat(String columnName) {
        return cellValueAsFloat(findCell(columnName), null);
    }

    @Override
    public Float getAsFloat(String columnName, Float defaultValue) {
        return cellValueAsFloat(findCell(columnName), defaultValue);
    }

    @Override
    public Double getAsDouble(String columnName) {
        return cellValueAsDouble(findCell(columnName), null);
    }

    @Override
    public Double getAsDouble(String columnName, Double defaultVelue) {
        return cellValueAsDouble(findCell(columnName), defaultVelue);
    }

    @Override
    public BigInteger getAsBigInteger(String columnName) {
        return cellValueAsBigInteger(findCell(columnName), null);
    }

    @Override
    public BigInteger getAsBigInteger(String columnName, BigInteger defaultValue) {
        return cellValueAsBigInteger(findCell(columnName), defaultValue);
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName) {
        return cellValueAsBigDecimal(findCell(columnName), null);
    }

    @Override
    public BigDecimal getAsBigDecimal(String columnName, BigDecimal defaultValue) {
        return cellValueAsBigDecimal(findCell(columnName), defaultValue);
    }

    @Override
    public Date getAsDate(String columnName) {
        return cellValueAsDate(findCell(columnName), null);
    }
    
    @Override
    public Date getAsDate(String columnName, Date defaultValue) {
        return cellValueAsDate(findCell(columnName), defaultValue);
    }
    
    @Override
    public LocalDate getAsLocalDate(String columnName) {
        return cellValueAsLocalDate(findCell(columnName), null);
    }
    
    @Override
    public LocalDate getAsLocalDate(String columnName, LocalDate defaultValue) {
        return cellValueAsLocalDate(findCell(columnName), defaultValue);
    }

    @Override
    public LocalDateTime getAsDateTime(String columnName) {
        return cellValueAsLocalDateTime(findCell(columnName), null);
    }

}

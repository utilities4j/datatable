package javax.util.data.datatable.excel;

import javax.util.data.datatable.RowWriter;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Calendar;

public class ExcelWriter implements RowWriter {

    private Workbook workbook;
    private Sheet sheet;
    private int rowIndex;
    private String filename;

    @Override
    public void initialize(String filenamte, List<String> columnNames) {
        this.filename = filenamte;
        this.workbook = new XSSFWorkbook();
        this.sheet = workbook.createSheet("Sheet1");
        this.rowIndex = 0;

        Row headerRow = sheet.createRow(rowIndex++);
        for (int i = 0; i < columnNames.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columnNames.get(i));
        }
    }

    @Override
    public void writeRow(Map<String, Object> row) {
        Row dataRow = sheet.createRow(rowIndex++);
        int cellIndex = 0;
        for (Object value : row.values()) {
            Cell cell = dataRow.createCell(cellIndex++);
            if (value == null) {
                cell.setCellValue("");
            } else if (value instanceof String string) {
                cell.setCellValue(string);
            } else if (value instanceof Integer integer) {
                cell.setCellValue(integer);
            } else if (value instanceof Double aDouble) {
                cell.setCellValue(aDouble);
            } else if (value instanceof Boolean aBoolean) {
                cell.setCellValue(aBoolean);
            } else if (value instanceof Calendar calendar) {
                cell.setCellValue(calendar);
            } else if (value instanceof java.sql.Date date) {
                cell.setCellValue(date);
            } else if (value instanceof java.util.Date date) {
                cell.setCellValue(date);
            } else if (value instanceof Float aFloat) {
                cell.setCellValue(aFloat);
            } else if (value instanceof Long aLong) {
                cell.setCellValue(aLong);
            } else if (value instanceof Number number) {
                cell.setCellValue(number.doubleValue());
            } else {
                cell.setCellValue(value.toString()); // Fallback para string se o tipo não for reconhecido
            }
        }
    }

    @Override
    public void finish() throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(filename)) {
            workbook.write(fileOut);
        }
        if (workbook != null) {
            workbook.close();
        }
    }
    
    public ByteArrayOutputStream toByteArrayOutputStream() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();        

        workbook.write(baos);

        if (workbook != null) {
            workbook.close();
        }

        return baos;
    }
}

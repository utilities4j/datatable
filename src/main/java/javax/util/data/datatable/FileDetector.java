package javax.util.data.datatable;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.apache.poi.poifs.filesystem.FileMagic;

public class FileDetector {

    private static final Pattern CSV_PATTERN = Pattern.compile("^(\"[^\"]*\"|[^,]*)(,(\"[^\"]*\"|[^,]*))*$");
// Enum para representar os tipos de arquivo suportados

    public enum FileType {
        CSV,
        XLSX,
        XLS,
        TXT,
        UNSUPPOTED
    }

    public static FileType getType(InputStream inputStream) throws IOException {
//        if (isCsvFile(inputStream)) {
//            return FileType.CSV;
//        }
//
//        if (isTextFile(inputStream)) {
//            return FileType.TXT;
//        }

        // Converte o InputStream para um array de bytes
        byte[] fileBytes = IOUtils.toByteArray(inputStream);

        // Cria um novo InputStream a partir do array de bytes
        try (InputStream byteArrayInputStream = new ByteArrayInputStream(fileBytes)) {
            // Detecta o tipo de arquivo usando FileMagic
            FileMagic fileMagic = FileMagic.valueOf(byteArrayInputStream);

            // Verifica se é um arquivo Excel (.xls ou .xlsx)
            if (fileMagic == FileMagic.OLE2) {
                return FileType.XLS;
            } else if (fileMagic == FileMagic.OOXML) {
                return FileType.XLSX;
            }
        }
        return FileType.UNSUPPOTED;        
    }

    public static boolean isTextFile(InputStream inputStream) throws IOException {
        // Converte o InputStream para um array de bytes
        byte[] fileBytes = IOUtils.toByteArray(inputStream);

        // Verifica se o conteúdo é ASCII ou UTF-8
        String content = new String(fileBytes, "UTF-8");
        if (content.matches("\\A\\p{ASCII}*\\z")) {
            return true;
        }

        return false;
    }

    public static boolean isCsvFile(InputStream inputStream) throws IOException {
        // Converte o InputStream para um array de bytes
        byte[] fileBytes = IOUtils.toByteArray(inputStream);

        // Usa um BufferedReader para ler as linhas do arquivo
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(fileBytes), "UTF-8"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                // Verifica se a linha corresponde ao padrão CSV
                if (!CSV_PATTERN.matcher(line).matches()) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {
        try (InputStream inputStream = FileDetector.class
                .getResourceAsStream("/path/to/your/file")) {
            boolean isText = isTextFile(inputStream);
            boolean isCsv = isCsvFile(new ByteArrayInputStream(IOUtils.toByteArray(inputStream)));

            System.out.println(
                    "Is Text file: " + isText);
            System.out.println(
                    "Is CSV file: " + isCsv);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package javax.util.data.datatable;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Xadrez
 */
public class UtilStringTest {
    
    

    /**
     * Test of higienizacao method, of class UtilString.
     */
    @Test
    public void testHigienizacao() {
        System.out.println("higienizacao");        
        
        assertEquals("ENDERECO_DE_ENTREGA", UtilString.higienizacao(" Endereço de entrega \n[varchar(40)] "));
        
    }
    
}

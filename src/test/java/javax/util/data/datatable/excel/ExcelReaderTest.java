package javax.util.data.datatable.excel;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Marcius
 */
public class ExcelReaderTest {

    public ExcelReaderTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of next method, of class ExcelReader.
     */
    @Test
    public void testHasNext() throws IOException {
        System.out.println("hasNext");
        ExcelReader excelReader = new ExcelReader("static/planilha-teste.xlsx");
        int count = 0;
        while (excelReader.hasNext()) {
            excelReader.next();
            count++;
            assertEquals(count, excelReader.getRowNumber());
            System.out.println("RowNumber " + excelReader.getRowNumber());
            System.out.println("Escola: " + excelReader.getAsString("ESCOLA"));
            System.out.println("Coord: " + excelReader.getAsInteger("Coord"));
            System.out.println("Candidato: " + excelReader.getAsInteger("candidato"));
        }

        assertEquals(99, count);
    }

    /**
     * Test of getRowNumber method, of class ExcelReader.
     */
    @Test
    public void testGetRowNumber() throws IOException {
        System.out.println("getRowNumber");
        ExcelReader excelReader = new ExcelReader("static/planilha-teste.xlsx");
        int count = 0;
        while (excelReader.next()) {            
            count++;
            assertEquals(count, excelReader.getRowNumber());
        }
    }

    /**
     * Test of columnNameExists method, of class ExcelReader.
     */
    @Test
    public void testColumnNameExists() throws IOException {
        System.out.println("columnNameExists");

        ExcelReader excelReader = new ExcelReader("static/planilha-teste.xlsx");

        assertEquals(true, excelReader.columnNameExists("ESCOLA"));
        assertEquals(true, excelReader.columnNameExists("ENDEREÇO"));//NOME ORIGINAL DA COLUNA
        assertEquals(true, excelReader.columnNameExists("ENDERECO"));//NOME DA COLUNA HIGIENIZADA
        assertEquals(true, excelReader.columnNameExists("SALA Escola"));
        assertEquals(true, excelReader.columnNameExists("SALA_ESCOLA"));//NOME DA COLUNA HIGIENIZADA
        assertEquals(true, excelReader.columnNameExists("3395")); //nome da coluna é um valor numerico       
        assertEquals(false, excelReader.columnNameExists("nm_pedido"));
        assertEquals(false, excelReader.columnNameExists("Condições Especiais"));
        assertEquals(false, excelReader.columnNameExists("ds_social\n[varchar(80)]"));
        assertEquals(false, excelReader.columnNameExists("ds_social"));
        assertEquals(false, excelReader.columnNameExists("DS_SOCIAL"));

        excelReader = new ExcelReader("static/planilha-teste-higienizacao-colunas.xlsx");
        assertEquals(false, excelReader.columnNameExists("ESCOLA"));
        assertEquals(false, excelReader.columnNameExists("ENDEREÇO"));
        assertEquals(false, excelReader.columnNameExists("ENDERECO"));
        assertEquals(false, excelReader.columnNameExists("SALA Escola"));
        assertEquals(false, excelReader.columnNameExists("SALA_ESCOLA"));
        assertEquals(false, excelReader.columnNameExists("3395"));
        assertEquals(true, excelReader.columnNameExists("nm_pedido"));
        assertEquals(true, excelReader.columnNameExists("Condições Especiais"));//NOME ORIGINAL DA COLUNA
        assertEquals(true, excelReader.columnNameExists("CondiCOES_ESPECIAIS"));//NOME DA COLUNA HIGIENIZADA
        assertEquals(true, excelReader.columnNameExists("ds_social\n[varchar(80)]"));
        assertEquals(true, excelReader.columnNameExists("ds_social"));//NOME DA COLUNA HIGIENIZADA
        assertEquals(true, excelReader.columnNameExists("DS_SOCIAL"));//NOME DA COLUNA HIGIENIZADA
    }

    /**
     * Test of verifyColumnsNames method, of class ExcelReader.
     */
//    @Test
//    public void testVerifyColumnsNames() {
//        System.out.println("verifyColumnsNames");
//        String[] columnNames = null;
//        ExcelReader instance = null;
//        Optional<String> expResult = null;
//        Optional<String> result = instance.verifyColumnsNames(columnNames);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of get method, of class ExcelReader.
     */
//    @Test
//    public void testGet() {
//        System.out.println("get");
//        String columnName = "";
//        ExcelReader instance = null;
//        Object expResult = null;
//        Object result = instance.get(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsString method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsString_String() {
//        System.out.println("getAsString");
//        String columnName = "";
//        ExcelReader instance = null;
//        String expResult = "";
//        String result = instance.getAsString(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsString method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsString_String_String() {
//        System.out.println("getAsString");
//        String columnName = "";
//        String defaultvalue = "";
//        ExcelReader instance = null;
//        String expResult = "";
//        String result = instance.getAsString(columnName, defaultvalue);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsInteger method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsInteger_String() {
//        System.out.println("getAsInteger");
//        String columnName = "";
//        ExcelReader instance = null;
//        Integer expResult = null;
//        Integer result = instance.getAsInteger(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsInteger method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsInteger_String_int() {
//        System.out.println("getAsInteger");
//        String columnName = "";
//        int defaultValue = 0;
//        ExcelReader instance = null;
//        int expResult = 0;
//        int result = instance.getAsInteger(columnName, defaultValue);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsLong method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsLong() {
//        System.out.println("getAsLong");
//        String columnName = "";
//        ExcelReader instance = null;
//        Long expResult = null;
//        Long result = instance.getAsLong(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsFloat method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsFloat() {
//        System.out.println("getAsFloat");
//        String columnName = "";
//        ExcelReader instance = null;
//        Float expResult = null;
//        Float result = instance.getAsFloat(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsDouble method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsDouble() {
//        System.out.println("getAsDouble");
//        String columnName = "";
//        ExcelReader instance = null;
//        Double expResult = null;
//        Double result = instance.getAsDouble(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsBigInteger method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsBigInteger() {
//        System.out.println("getAsBigInteger");
//        String columnName = "";
//        ExcelReader instance = null;
//        BigInteger expResult = null;
//        BigInteger result = instance.getAsBigInteger(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsBigDecimal method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsBigDecimal() {
//        System.out.println("getAsBigDecimal");
//        String columnName = "";
//        ExcelReader instance = null;
//        BigDecimal expResult = null;
//        BigDecimal result = instance.getAsBigDecimal(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsDate method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsDate() {
//        System.out.println("getAsDate");
//        String columnName = "";
//        ExcelReader instance = null;
//        LocalDate expResult = null;
//        LocalDate result = instance.getAsDate(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of getAsDateTime method, of class ExcelReader.
     */
//    @Test
//    public void testGetAsDateTime() {
//        System.out.println("getAsDateTime");
//        String columnName = "";
//        ExcelReader instance = null;
//        LocalDateTime expResult = null;
//        LocalDateTime result = instance.getAsDateTime(columnName);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
